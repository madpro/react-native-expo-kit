import {Dimensions} from 'react-native';

const {height} = Dimensions.get('window');

const styles = {
  main: {
    marginTop: ((height / 2) - 100),
    justifyContent: 'center',
    alignItems: 'center',
  },
};

export default styles;
