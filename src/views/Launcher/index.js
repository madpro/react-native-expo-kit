/**
 * @flow
 */
import React, {Component} from 'react';
// import {NavigationActions} from 'react-navigation'
import {Container, View, Spinner, H1} from 'native-base';
import styles from './styles';

export default class LauncherView extends Component {
  // componentDidMount() {
  //     const {navigation} = this.props;
  //     setTimeout(() => {
  //         const resetAction = NavigationActions.reset({
  //             index: 0,
  //             actions: [
  //                 NavigationActions.navigate({routeName: 'Main'})
  //             ]
  //         });
  //         navigation.dispatch(resetAction);
  //     }, 1000);
  // }

  render() {
    return (
      <Container>
        <View style={styles.main}>
          <H1>React Native starter kit</H1>
          <Spinner color="grey"/>
        </View>
      </Container>);
  }
}
