import LauncherView from './views/Launcher';

export const Navigation = {
  Launcher: {screen: LauncherView},
};

export const Options = {
  initialRouteName: 'Launcher',
  headerMode: 'none',
};
