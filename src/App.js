// import React from 'react';
import {StackNavigator} from 'react-navigation';
import {Navigation, Options} from './Navigation';


const App = StackNavigator(Navigation, Options);

export default App;
