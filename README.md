# React Native starter expo kit

## Required
- node.js 6.+
- npm 5.+
- exp - `npm install -g exp`

## Libs
- react
- react-native
- expo
- native-base
- react-native-vector-icons
- react-navigation

## Dev
- eslint
- pre-commit


## Development
- `find . -type f -exec sed -i '' -e 's/#starter-kit#/my-super-project/g' {} \; ` - change project name from #starter-kit#
- `npm i` - install packages
- `npm start` - start project
- `npm run eslint` - linter check
