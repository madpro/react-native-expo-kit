// import React from 'react';
import {StackNavigator} from 'react-navigation';
import {Font} from 'expo';

import {Navigation, Options} from './src/Navigation';

const EvilIcons = require('./assets/fonts/EvilIcons.ttf');


Font.loadAsync({
  EvilIcons,
});

const App = StackNavigator(Navigation, Options);

export default App;
